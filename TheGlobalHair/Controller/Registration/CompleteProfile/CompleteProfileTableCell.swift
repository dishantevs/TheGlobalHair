//
//  CompleteProfileTableCell.swift
//  TheGlobalHair
//
//  Created by Apple on 18/11/20.
//

import UIKit


class CompleteProfileTableCell: UITableViewCell {

    @IBOutlet weak var txtAddress:UITextField! {
        didSet {
            txtAddress.layer.cornerRadius = 6
            txtAddress.clipsToBounds = true
            txtAddress.setLeftPaddingPoints(20)
            txtAddress.layer.borderColor = UIColor.clear.cgColor
            txtAddress.layer.borderWidth = 0.8
            txtAddress.keyboardAppearance = .dark
            txtAddress.placeholder = "Address"
            txtAddress.textColor = .white
            txtAddress.backgroundColor = UIColor.init(red: 43.0/255.0, green: 100.0/255.0, blue: 191.0/255.0, alpha: 1) // 43 100 191
        }
    }
    
    @IBOutlet weak var btnCountryClick:UIButton!
    @IBOutlet weak var txtCountry:UITextField! {
        didSet {
            txtCountry.layer.cornerRadius = 6
            txtCountry.clipsToBounds = true
            txtCountry.setLeftPaddingPoints(20)
            txtCountry.layer.borderColor = UIColor.clear.cgColor
            txtCountry.layer.borderWidth = 0.8
            txtCountry.keyboardAppearance = .dark
            txtCountry.placeholder = "Country"
            txtCountry.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var btnNumberOfProducts:UIButton!
    @IBOutlet weak var txtNumberOfProducts:UITextField! {
        didSet {
            txtNumberOfProducts.layer.cornerRadius = 6
            txtNumberOfProducts.clipsToBounds = true
            txtNumberOfProducts.setLeftPaddingPoints(20)
            txtNumberOfProducts.layer.borderColor = UIColor.clear.cgColor
            txtNumberOfProducts.layer.borderWidth = 0.8
            txtNumberOfProducts.keyboardAppearance = .dark
            txtNumberOfProducts.placeholder = "Number of Products"
            txtNumberOfProducts.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var btnStoreTimeFrom:UIButton!
    @IBOutlet weak var txtStoreTimeFrom:UITextField! {
        didSet {
            txtStoreTimeFrom.layer.cornerRadius = 6
            txtStoreTimeFrom.clipsToBounds = true
            txtStoreTimeFrom.setLeftPaddingPoints(20)
            txtStoreTimeFrom.layer.borderColor = UIColor.clear.cgColor
            txtStoreTimeFrom.layer.borderWidth = 0.8
            txtStoreTimeFrom.keyboardAppearance = .dark
            txtStoreTimeFrom.placeholder = "Store Time From"
            txtStoreTimeFrom.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var btnStoreTimeTo:UIButton!
    @IBOutlet weak var txtStoreTimeTo:UITextField! {
        didSet {
            txtStoreTimeTo.layer.cornerRadius = 6
            txtStoreTimeTo.clipsToBounds = true
            txtStoreTimeTo.setLeftPaddingPoints(20)
            txtStoreTimeTo.layer.borderColor = UIColor.clear.cgColor
            txtStoreTimeTo.layer.borderWidth = 0.8
            txtStoreTimeTo.keyboardAppearance = .dark
            txtStoreTimeTo.placeholder = "Store Time To"
            txtStoreTimeTo.backgroundColor = .white
        }
    }
    
    @IBOutlet weak var txtStorePhoneNumber:UITextField! {
        didSet {
            txtStorePhoneNumber.layer.cornerRadius = 6
            txtStorePhoneNumber.clipsToBounds = true
            txtStorePhoneNumber.setLeftPaddingPoints(20)
            txtStorePhoneNumber.layer.borderColor = UIColor.clear.cgColor
            txtStorePhoneNumber.layer.borderWidth = 0.8
            txtStorePhoneNumber.keyboardAppearance = .dark
            txtStorePhoneNumber.placeholder = "Store Phone Number"
            txtStorePhoneNumber.backgroundColor = .white
            txtStorePhoneNumber.keyboardType = .phonePad
        }
    }
    
    @IBOutlet weak var btnSaveAndContinue:UIButton! {
        didSet {
            btnSaveAndContinue.backgroundColor = NAVIGATION_COLOR
            btnSaveAndContinue.layer.cornerRadius = 6
            btnSaveAndContinue.setTitleColor(APP_BASIC_COLOR, for: .normal)
            btnSaveAndContinue.setTitle("Save & Continue", for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
