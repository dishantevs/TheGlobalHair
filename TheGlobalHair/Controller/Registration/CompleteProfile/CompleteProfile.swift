//
//  CompleteProfile.swift
//  TheGlobalHair
//
//  Created by Apple on 18/11/20.
//

import UIKit
import Alamofire

// MARK:- LOCATION -
import CoreLocation

class CompleteProfile: UIViewController, CLLocationManagerDelegate {

// ***************************************************************** // nav
            
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_COLOR
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.tintColor = NAVIGATION_TITLE_COLOR
        }
    }
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "Complete Profile"
            lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
        }
    }
            
// ***************************************************************** // nav
    
    let cellReuseIdentifier = "completeProfileTableCell"
    
    let locationManager = CLLocationManager()
    
    // save total quantity list
    var totalCountryList:NSMutableArray! = []
    var totalCountryListName:NSMutableArray! = []
    var totalProductQuantity:NSMutableArray! = []
    
    // MARK:- SELECT GENDER -
    let regularFont = UIFont.systemFont(ofSize: 16)
    let boldFont = UIFont.boldSystemFont(ofSize: 16)
    
    // MARK:- SAVE LOCATION STRING -
    var strSaveLatitude:String!
    var strSaveLongitude:String!
    var strSaveCountryName:String!
    var strSaveLocalAddress:String!
    var strSaveLocality:String!
    var strSaveLocalAddressMini:String!
    var strSaveStateName:String!
    var strSaveZipcodeName:String!
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    
    
// ***************************************************************** //
// ***************************************************************** //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.strSaveLatitude = "0"
        self.strSaveLongitude = "0"
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.view.backgroundColor = .black
        
        self.countryWebservice()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.iAmHereForLocationPermission()
    }
    
    @objc func backClickMethod() {
        
        for controller in self.navigationController!.viewControllers as Array {
        if controller.isKind(of: GetStarted.self) {
            self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
        
        // self.navigationController?.popViewController(animated: true)
    }

    // MARK:- KEYBOARD WILL SHOW -
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    // MARK:- KEYBOARD WILL HIDE -
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    @objc func iAmHereForLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
              
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.strSaveLatitude = "0"
                self.strSaveLongitude = "0"
                
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                          
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                      
            @unknown default:
                break
            }
        }
    }
    
    // MARK:- GET CUSTOMER LOCATION -
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
         let indexPath = IndexPath.init(row: 0, section: 0)
         let cell = self.tbleView.cellForRow(at: indexPath) as! CompleteProfileTableCell
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        location.fetchCityAndCountry { city, country, zipcode,localAddress,localAddressMini,locality, error in
            guard let city = city, let country = country,let zipcode = zipcode,let localAddress = localAddress,let localAddressMini = localAddressMini,let locality = locality, error == nil else { return }
            
            self.strSaveCountryName     = country
            self.strSaveStateName       = city
            self.strSaveZipcodeName     = zipcode
            
            self.strSaveLocalAddress     = localAddress
            self.strSaveLocality         = locality
            self.strSaveLocalAddressMini = localAddressMini
            
            //print(self.strSaveLocality+" "+self.strSaveLocalAddress+" "+self.strSaveLocalAddressMini)
            
            let doubleLat = locValue.latitude
            let doubleStringLat = String(doubleLat)
            
            let doubleLong = locValue.longitude
            let doubleStringLong = String(doubleLong)
            
            self.strSaveLatitude = String(doubleStringLat)
            self.strSaveLongitude = String(doubleStringLong)
            
            print("local address ==> "+localAddress as Any) // south west delhi
            print("local address mini ==> "+localAddressMini as Any) // new delhi
            print("locality ==> "+locality as Any) // sector 10 dwarka
            
            print(self.strSaveCountryName as Any) // india
            print(self.strSaveStateName as Any) // new delhi
            print(self.strSaveZipcodeName as Any) // 110075
            
            //MARK:- STOP LOCATION -
            self.locationManager.stopUpdatingLocation()
            
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.reloadData()
            
            cell.txtAddress.text = self.strSaveLocality+", "+self.strSaveLocalAddressMini+", "+self.strSaveZipcodeName //+","+strSaveLocalAddress+","+strSaveLocalAddressMini+"-"+strSaveZipcodeName
            
            cell.txtCountry.text = self.strSaveCountryName
            
            
            // self.findMyStateTaxWB()
        }
    }
    
    
    @objc func validationBeforeCompleteRegsitration() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! CompleteProfileTableCell
        
        if cell.txtCountry.text == "" {
            
            let alert = UIAlertController(title: String("Alert"), message: String("Country")+" should not be empty", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if cell.txtNumberOfProducts.text == "" {
            
            let alert = UIAlertController(title: String("Alert"), message: String("Number of products")+" should not be empty", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if cell.txtStoreTimeFrom.text == "" {
            
            let alert = UIAlertController(title: String("Alert"), message: String("Store time from")+" should not be empty", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if cell.txtStoreTimeTo.text == "" {
            
            let alert = UIAlertController(title: String("Alert"), message: String("Store time to")+" should not be empty", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else if cell.txtStorePhoneNumber.text == "" {
            
            let alert = UIAlertController(title: String("Alert"), message: String("Store phone number")+" should not be empty", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            self.completeProfileWB()
        }
    }
    
    @objc func completeProfileWB() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! CompleteProfileTableCell
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         // let str:String = person["role"] as! String
        
            let x : Int = person["userId"] as! Int
            let myString = String(x)
            
        let params = EditRegsitration(action: "editprofile",
                                      userId: String(myString),
                                      country: String(cell.txtCountry.text!),
                                      NoOfProduct: String(cell.txtNumberOfProducts.text!),
                                      StoreTime: String(cell.txtStoreTimeFrom.text!)+" TO "+String(cell.txtStoreTimeTo.text!),
                                      contactNumber: String(cell.txtStorePhoneNumber.text!))
        
        print(params as Any)
        
        AF.request(BASE_URL_THE_GLOBAL_HAIR,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            var dict: Dictionary<AnyHashable, Any>
                            dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                            
                             let defaults = UserDefaults.standard
                             defaults.setValue(dict, forKey: "keyLoginFullData")
                            
                             let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MembershipId") as? Membership
                             self.navigationController?.pushViewController(push!, animated: true)
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
                   }
    }
        
    }
    
}

extension CompleteProfile: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:CompleteProfileTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! CompleteProfileTableCell
          
        cell.btnSaveAndContinue.addTarget(self, action: #selector(saveAndContinueClickMethod), for: .touchUpInside)
        
        cell.btnCountryClick.addTarget(self, action: #selector(countryClickMethod), for: .touchUpInside)
        
        cell.btnNumberOfProducts.addTarget(self, action: #selector(numberOfProductClickMethod), for: .touchUpInside)
        
        cell.btnStoreTimeFrom.addTarget(self, action: #selector(showTimeStoreFromClickMethod), for: .touchUpInside)
        
        cell.btnStoreTimeTo.addTarget(self, action: #selector(showTimeStoreToClickMethod), for: .touchUpInside)
        
        cell.backgroundColor = .clear
        
        return cell
        
    }

    @objc func countryWebservice () {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        // self.arrListOfAllMyOrders.removeAllObjects()
        
        self.view.endEditing(true)
        
        let params = CountryList(action: "countrylist")
        
        AF.request(BASE_URL_THE_GLOBAL_HAIR,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                         // print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.totalCountryList.addObjects(from: ar as! [Any])
                            
                            // self.tbleView.delegate = self
                            // self.tbleView.dataSource = self
                            // self.tbleView.reloadData()
                            
                            // self.totalItemsInCart()
                            // self.indicatorr.startAnimating()
                            
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
        
    }
    
    @objc func countryClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! CompleteProfileTableCell
        
        for myCountryCheck in 0..<self.totalCountryList.count {
            
            let item = self.totalCountryList[myCountryCheck] as! [String:Any]
            
            self.totalCountryListName.add(item["name"] as! String)
        }
        
        let redAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "Select Country",
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : NAVIGATION_COLOR,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "search country",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Okay",
            doneButtonColor     : APP_BASIC_COLOR,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"red_ic_checked"),
            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
             
            // MARK:- CONVERT MUTABLE ARRAY TO ARRAY -
        let array: [String] = self.totalCountryListName.copy() as! [String]
            
        let arrGender = array
        let picker = YBTextPicker.init(with: arrGender, appearance: redAppearance,
                                       onCompletion: { (selectedIndexes, selectedValues) in
                                        if let selectedValue = selectedValues.first{
                                            if selectedValue == arrGender.last!{
                                                     
                                                    // self.productQuantityWithTextSaved = "\(selectedValue)"
                                                    
                                                cell.txtCountry.text = "\(selectedValue)"
                                                self.tbleView.reloadData()
                                                    
                                            } else {
                                                     
                                                    // self.productQuantityWithTextSaved = "\(selectedValue)"
                                                cell.txtCountry.text = "\(selectedValue)"
                                                self.tbleView.reloadData()
                                            }
                                        } else {
                                                 // self.btnGenderPicker.setTitle("What's your gender?", for: .normal)
                                                  cell.txtCountry.text = "Please select your country"
                                                
                                            print()
                                                
                                        }
                                       },
                                       onCancel: {
                                        print("Cancelled")
                                       })
             
        picker.show(withAnimation: .FromBottom)
    }
    
    @objc func numberOfProductClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! CompleteProfileTableCell
        
        for quantity in 1..<11 {
            
            print(quantity as Any)
            
            let x : Int = quantity
            let myString = String(x)
            
             self.totalProductQuantity.add(myString)
        }
        
        
        
        let redAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "Select Number of Products",
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : NAVIGATION_COLOR,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "search products",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Okay",
            doneButtonColor     : APP_BASIC_COLOR,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"red_ic_checked"),
            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
             
           
        // MARK:- CONVERT MUTABLE ARRAY TO ARRAY -
    let array: [String] = self.totalProductQuantity.copy() as! [String]
        
    let arrGender = array
        
        // let arrGender = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
        
        let picker = YBTextPicker.init(with: arrGender, appearance: redAppearance,
                                       onCompletion: { (selectedIndexes, selectedValues) in
                                        if let selectedValue = selectedValues.first{
                                            if selectedValue == arrGender.last!{
                                                     
                                                    // self.productQuantityWithTextSaved = "\(selectedValue)"
                                                    
                                                cell.txtNumberOfProducts.text = "\(selectedValue)"
                                                self.tbleView.reloadData()
                                                    
                                            } else {
                                                     
                                                    // self.productQuantityWithTextSaved = "\(selectedValue)"
                                                cell.txtNumberOfProducts.text = "\(selectedValue)"
                                                self.tbleView.reloadData()
                                            }
                                        } else {
                                                 // self.btnGenderPicker.setTitle("What's your gender?", for: .normal)
                                                  cell.txtNumberOfProducts.text = "Please select product quantity"
                                                
                                            print()
                                                
                                        }
                                       },
                                       onCancel: {
                                        print("Cancelled")
                                       })
             
        picker.show(withAnimation: .FromBottom)
    }
    
    @objc func showTimeStoreFromClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! CompleteProfileTableCell
        
        RPicker.selectDate(title: "Select Time", cancelText: "Cancel", datePickerMode: .time, didSelectDate: { [weak self](selectedDate) in
            // TODO: Your implementation for date
            // self?.outputLabel.text = selectedDate.dateString("hh:mm a")
            cell.txtStoreTimeFrom.text = selectedDate.dateString("hh:mm a")// "\(selectedDate.dateString("hh:mm a"))"
            self!.tbleView.reloadData()
        })
    }
    
    @objc func showTimeStoreToClickMethod() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! CompleteProfileTableCell
        
        if cell.txtStoreTimeFrom.text == "" {
            
            let alert = UIAlertController(title: String("Alert"), message: String("Please select From date first."), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            RPicker.selectDate(title: "Select Time", cancelText: "Cancel", datePickerMode: .time, didSelectDate: { [weak self](selectedDate) in
                // TODO: Your implementation for date
                // self?.outputLabel.text = selectedDate.dateString("hh:mm a")
                cell.txtStoreTimeTo.text = selectedDate.dateString("hh:mm a")// "\(selectedDate.dateString("hh:mm a"))"
                self!.tbleView.reloadData()
            })
            
        }
        
    }
    
    func dateSelect()  {

        //init date picker
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 260))
        datePicker.datePickerMode = UIDatePicker.Mode.date

        //add target
        datePicker.addTarget(self, action: #selector(dateSelected(datePicker:)), for: UIControl.Event.valueChanged)

        //add to actionsheetview
        let alertController = UIAlertController(title: "", message:" " , preferredStyle: UIAlertController.Style.actionSheet)

        alertController.view.addSubview(datePicker)//add subview

        let cancelAction = UIAlertAction(title: "Done", style: .cancel) { (action) in
            self.dateSelected(datePicker: datePicker)
        }

        //add button to action sheet
        alertController.addAction(cancelAction)

        let height:NSLayoutConstraint = NSLayoutConstraint(item: alertController.view as Any, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 300)
        alertController.view.addConstraint(height);

        self.present(alertController, animated: true, completion: nil)

    }


    //selected date func
    @objc func dateSelected(datePicker:UIDatePicker) {

        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short

        let currentDate = datePicker.date

        print(currentDate)

    }
    
    @objc func saveAndContinueClickMethod() {
        self.validationBeforeCompleteRegsitration()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 700
    }
    
}

extension CompleteProfile: UITableViewDelegate {
    
}
