//
//  Advertiser.swift
//  TheGlobalHair
//
//  Created by Apple on 18/11/20.
//

import UIKit

class Advertiser: UIViewController {

// ***************************************************************** // nav
                    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_COLOR
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.tintColor = NAVIGATION_BACK_COLOR
        }
    }
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "Advertiser"
            lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
        }
    }
                    
// ***************************************************************** // nav
    
    let cellReuseIdentifier = "advertiserTableCell"
    
    let dummyArray = ["One","Two","Three","Four"]
    
    var dictGetMembershipData:NSDictionary! = nil
    
    @IBOutlet weak var lblMessage:UILabel! {
        didSet {
            lblMessage.text = "Please select a types of advertisers."
            lblMessage.textColor = APP_BASIC_COLOR
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = .black
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension Advertiser: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dummyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:AdvertiserTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! AdvertiserTableCell
          
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
         cell.lblTitle.text = " "+dummyArray[indexPath.row]
        // cell.txtView.text = "\nProduct Listing : 500\nPhone Support : Yes\nRating : Yes\n"
        
        // cell.btnCheckUncheck.isHidden = true
        // cell.btnCheckUncheck.tag = indexPath.row
        
        cell.backgroundColor = .clear
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        // let item = dummyArray[indexPath.row]
        
        let alert = UIAlertController(title: String("Item"), message: String("Are you sure you want to select this item ?"), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
             
            let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CSelectPaymetScreenId") as? CSelectPaymetScreen
            settingsVCId!.dictGetDataForPayment = self.dictGetMembershipData
            settingsVCId!.strPaymentType = "2"
            self.navigationController?.pushViewController(settingsVCId!, animated: true)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
             
        }))
         
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 75 // UITableView.automaticDimension
    }
    
}

extension Advertiser: UITableViewDelegate {
    
}
