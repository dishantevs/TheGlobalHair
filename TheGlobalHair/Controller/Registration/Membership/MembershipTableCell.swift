//
//  MembershipTableCell.swift
//  TheGlobalHair
//
//  Created by Apple on 18/11/20.
//

import UIKit

class MembershipTableCell: UITableViewCell {

    @IBOutlet weak var viewBG:UIView! {
        didSet {
            viewBG.layer.cornerRadius = 6
            viewBG.clipsToBounds = true
            viewBG.backgroundColor = .clear
        }
    }
    
    @IBOutlet weak var lblTitle:UILabel! {
        didSet {
            lblTitle.textAlignment = .left
            lblTitle.layer.cornerRadius = 6
            lblTitle.clipsToBounds = true
            lblTitle.backgroundColor = UIColor.init(red: 43.0/255.0, green: 100.0/255.0, blue: 191.0/255.0, alpha: 1)
            lblTitle.textColor = APP_BASIC_COLOR
        }
    }
    
    @IBOutlet weak var txtView:UILabel! {
        didSet {
            txtView.textAlignment = .center
            txtView.layer.cornerRadius = 0
            txtView.clipsToBounds = true
            txtView.backgroundColor = UIColor.init(red: 29.0/255.0, green: 70.0/255.0, blue: 133.0/255.0, alpha: 1)
            txtView.textColor = APP_BASIC_COLOR
        }
    }
    
    @IBOutlet weak var btnCheckUncheck:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
