//
//  Membership.swift
//  TheGlobalHair
//
//  Created by Apple on 18/11/20.
//

import UIKit
import Alamofire

class Membership: UIViewController {

// ***************************************************************** // nav
                
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_COLOR
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.tintColor = NAVIGATION_BACK_COLOR
        }
    }
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "Membership"
            lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
        }
    }
                
// ***************************************************************** // nav
    
    let cellReuseIdentifier = "membershipTableCell"
    
    let arrTitleWithPrice = ["Gold - $299","Silver - $199","Platinum - $399"]
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var strPhoneSupport:String!
    var strRating:String!
    
    @IBOutlet weak var lblMessage:UILabel! {
        didSet {
            lblMessage.text = "Please select a membership subscription to list your products."
            lblMessage.textColor = APP_BASIC_COLOR
        }
    }
    
    @IBOutlet weak var btnContinue:UIButton! {
        didSet {
            btnContinue.backgroundColor = NAVIGATION_COLOR
            btnContinue.setTitle("Continue", for: .normal)
            btnContinue.setTitleColor(APP_BASIC_COLOR, for: .normal)
            btnContinue.isHidden = true
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = .black
        
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.btnContinue.addTarget(self, action: #selector(continueClickMethod), for: .touchUpInside)
        
        self.membershipList()
    }

    @objc func backClickMethod() {
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // print(person as Any)
            
            if person["role"] as! String == "Seller" {
                                       
                if person["country"] as! String == "" {
                    
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    
                    for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: GetStarted.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                            break
                        }
                    }
                    
                }
            } else {
                // customer
            }
        }
        
    }

    @objc func continueClickMethod() {
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AdvertiserId") as? Advertiser
        self.navigationController?.pushViewController(push!, animated: true)
    }
    
    
    @objc func membershipList() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        // self.arrListOfAllMyOrders.removeAllObjects()
        
        self.view.endEditing(true)
        
        let forgotPasswordP = MembershipSub(action: "subscriptions")
        
        AF.request(BASE_URL_THE_GLOBAL_HAIR,
                   method: .post,
                   parameters: forgotPasswordP,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                         print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            self.tbleView.delegate = self
                            self.tbleView.dataSource = self
                            self.tbleView.reloadData()
                            
                            // self.totalItemsInCart()
                            // self.indicatorr.startAnimating()
                            
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
        
    }
}

extension Membership: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:MembershipTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! MembershipTableCell
          
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        let item = self.arrListOfAllMyOrders[indexPath.row] as! [String:Any]
        cell.lblTitle.text = " "+(item["name"] as! String)
        
        let x : Int = item["ProductLlisting"] as! Int
        let myString = String(x)
        
        let x2 : Int = item["PhoneSupport"] as! Int
        let myString2 = String(x2)
        
        if myString2 == "1" {
            self.strPhoneSupport = "YES"
        } else {
            self.strPhoneSupport = "NO"
        }
        
        let x3 : Int = item["Rating"] as! Int
        let myString3 = String(x3)
        
        if myString3 == "1" {
            self.strRating = "YES"
        } else {
            self.strRating = "NO"
        }
        
        cell.txtView.text = "Product Listing : "+myString+"\nPhone Support : "+strPhoneSupport+"\nRating : "+strRating
        
        cell.btnCheckUncheck.isHidden = true
        cell.btnCheckUncheck.tag = indexPath.row
        
        cell.backgroundColor = .clear
        
        return cell
        
    }
    
    // unchecked2,3

    /*
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        // tableView .deselectRow(at: indexPath, animated: true)
        
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
        }
    }
    */
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        let item = self.arrListOfAllMyOrders[indexPath.row] as! [String:Any]
        
        let alert = UIAlertController(title: String("Membership Plan"), message: "Plan Name : "+(item["name"] as! String), preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes, Pay", style: .default, handler: { action in
            let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AdvertiserId") as? Advertiser
            push!.dictGetMembershipData = item as NSDictionary
            self.navigationController?.pushViewController(push!, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
        
        }))
        
        self.present(alert, animated: true, completion: nil)
        
        /*
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark

        }
         */
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return UITableView.automaticDimension
    }
    
}

extension Membership: UITableViewDelegate {
    
}
