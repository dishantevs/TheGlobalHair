//
//  CShopNow.swift
//  TheGlobalHair
//
//  Created by Apple on 19/11/20.
//

import UIKit
import Alamofire

class CShopNow: UIViewController {
    
// ***************************************************************** // nav
                        
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_COLOR
        }
    }
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.tintColor = NAVIGATION_BACK_COLOR
        }
    }
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "Shop Now"
            lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
        }
    }
                        
// ***************************************************************** // nav
        
        let cellReuseIdentifier = "cShopNowTableCell"
        
        let dummyArray = ["One","Two","Three","Four"]
        
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    @IBOutlet weak var btnCart:UIButton! {
        didSet {
            btnCart.backgroundColor = .clear
            btnCart.tintColor = .black
            btnCart.isHidden = true
        }
    }
    
    @IBOutlet weak var lblCartCount:UILabel! {
        didSet {
            lblCartCount.isHidden = true
            lblCartCount.textColor = CART_COUNT_COLOR
        }
    }
    
        @IBOutlet weak var lblMessage:UILabel! {
            didSet {
                lblMessage.text = "Please select a types of advertisers."
                lblMessage.textColor = APP_BASIC_COLOR
            }
        }
        
    @IBOutlet weak var indicatorr:UIActivityIndicatorView! {
        didSet {
            indicatorr.color = .white
        }
    }
    
        // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
                // self.tbleView.delegate = self
                // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
            
        self.view.backgroundColor = .black
            
        // self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        if let myLoadedString = UserDefaults.standard.string(forKey: "keyBackOrSlide") {
            if myLoadedString == "backOrMenu" {
                
                // menu
                btnBack.setImage(UIImage(named: "menu"), for: .normal)
                self.sideBarMenuClick()
            } else {
                
                // back
                btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
                btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
            }
            
         } else {
            
            // back
            btnBack.setImage(UIImage(systemName: "arrow.left"), for: .normal)
            btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.categoriesWB()
    }
    
    @objc func backClickMethod() {
        
        self.navigationController?.popViewController(animated: true)
    }
 
    @objc func sideBarMenuClick() {
        
        let defaults = UserDefaults.standard
        defaults.setValue("", forKey: "keyBackOrSlide")
        defaults.setValue(nil, forKey: "keyBackOrSlide")
        
        if revealViewController() != nil {
        btnBack.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
          }
    }
    
    // MARK:- WEBSERVICE ( CATEGORIES ) -
    @objc func categoriesWB() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        self.arrListOfAllMyOrders.removeAllObjects()
        
        self.view.endEditing(true)
        
        let forgotPasswordP = ShopNowParam(action: "category")
        
        AF.request(BASE_URL_THE_GLOBAL_HAIR,
                   method: .post,
                   parameters: forgotPasswordP,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        // print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            self.tbleView.delegate = self
                            self.tbleView.dataSource = self
                            self.tbleView.reloadData()
                            
                            self.totalItemsInCart()
                            self.indicatorr.startAnimating()
                            
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
        
    }
    
    
    
    
    // MARK:- WEBSERVICE ( TOTAL ITEMS IN CART ) -
    @objc func totalItemsInCart() {
        // ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         let x : Int = (person["userId"] as! Int)
         let myString = String(x)
            
        let params = CartList(action: "getcarts",
                              userId: String(myString))
        
        AF.request(BASE_URL_THE_GLOBAL_HAIR,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            // var ar : NSArray!
                            // ar = (JSON["data"] as! Array<Any>) as NSArray
                            // self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            // var strCartCount : String!
                            // strCartCount = JSON["TotalCartItem"]as Any as? String
                            // print(strCartCount as Any)
                            
                            // cart
                            
                            let x : Int = JSON["TotalCartItem"] as! Int
                            let myString = String(x)
                            
                            if myString == "0" {
                                self.lblCartCount.isHidden = true
                                self.btnCart.isHidden = true
                            } else if myString == "" {
                                self.lblCartCount.isHidden = true
                                self.btnCart.isHidden = true
                            } else {
                                self.lblCartCount.isHidden = false
                                self.lblCartCount.text = String(myString)
                                
                                self.btnCart.isHidden = false
                                self.btnCart.setImage(UIImage(systemName: "cart.fill"), for: .normal)
                                
                                self.btnCart.addTarget(self, action: #selector(self.showCartlistClickMethod), for: .touchUpInside)
                            }
                            
                            self.indicatorr.stopAnimating()
                            self.indicatorr.hidesWhenStopped = true
                            
                            /*
                            if JSON["TotalCartItem"] is String {
                                print("Yes, it's a String")

                                self.lblCartCount.text = ""

                            } else if JSON["TotalCartItem"] is Int {
                                print("It is Integer")
                                            
                                let x2 : Int = (JSON["TotalCartItem"] as! Int)
                                let myString2 = String(x2)
                                self.lblCartCount.text = myString2
                                            
                            } else {
                                print("i am number")
                                            
                                let temp:NSNumber = JSON["TotalCartItem"] as! NSNumber
                                let tempString = temp.stringValue
                                self.lblCartCount.text = tempString
                            }
                            */
                            
                            
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            self.indicatorr.stopAnimating()
                            self.indicatorr.hidesWhenStopped = true
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        self.indicatorr.stopAnimating()
                        self.indicatorr.hidesWhenStopped = true
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
        
    }
}
    
    @objc func showCartlistClickMethod() {
         let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CTotalCartListId") as? CTotalCartList
         self.navigationController?.pushViewController(settingsVCId!, animated: true)
    }
    
}


extension CShopNow: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:CShopNowTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! CShopNowTableCell
          
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        // print(item as Any)
        
        cell.lblTitle.text = (item!["name"] as! String)
        
        // cell.txtView.text = "\nProduct Listing : 500\nPhone Support : Yes\nRating : Yes\n"
        
        // cell.btnCheckUncheck.isHidden = true
        // cell.btnCheckUncheck.tag = indexPath.row
        
        cell.backgroundColor = .clear
        
        return cell
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        let settingsVCId = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CShopNowCategoryId") as? CShopNowCategory
        settingsVCId!.dictOrderDetails = item as NSDictionary?
        self.navigationController?.pushViewController(settingsVCId!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 75 // UITableView.automaticDimension
    }
    
}

extension CShopNow: UITableViewDelegate {
    
}
