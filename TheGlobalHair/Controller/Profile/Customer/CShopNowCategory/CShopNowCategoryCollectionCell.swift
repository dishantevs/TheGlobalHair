//
//  CShopNowCategoryCollectionCell.swift
//  TheGlobalHair
//
//  Created by Apple on 19/11/20.
//

import UIKit

class CShopNowCategoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCellBG:UIView! {
        didSet {
            viewCellBG.layer.cornerRadius = 6
            viewCellBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var imgImage:UIImageView!
    
}
