//
//  COrderDetailsCollectionCell.swift
//  Alien Broccoli
//
//  Created by Apple on 29/09/20.
//

import UIKit

class COrderDetailsCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var viewCellBG:UIView! {
        didSet {
            viewCellBG.layer.cornerRadius = 8
            viewCellBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var imgProductImage:UIImageView! {
        didSet {
            imgProductImage.layer.cornerRadius = 0
            imgProductImage.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblTitle:UILabel! {
        didSet {
            lblTitle.textColor = APP_BASIC_COLOR
        }
    }
    
    @IBOutlet weak var lblPrice:UILabel! {
        didSet {
            lblPrice.textColor = APP_BASIC_COLOR
            lblPrice.layer.cornerRadius = 0
            lblPrice.clipsToBounds = true
            lblPrice.layer.borderWidth = 0.6
            lblPrice.layer.borderColor = UIColor.lightGray.cgColor
            
            let attrString = NSAttributedString(string: "Label Text", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
            lblPrice.attributedText = attrString
        }
    }
    
    @IBOutlet weak var lblSpecialPrice:UILabel! {
        didSet {
            lblSpecialPrice.textColor = APP_BASIC_COLOR
        }
    }
    
}
