//
//  TGAddProductTableCell.swift
//  TheGlobalHair
//
//  Created by Apple on 19/11/20.
//

import UIKit

class TGAddProductTableCell: UITableViewCell {

    @IBOutlet weak var txtProductName:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtProductName,
                              tfName: txtProductName.text!,
                              tfCornerRadius: 6,
                              tfpadding: 20,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .clear,
                              tfAppearance: .dark,
                              tfKeyboardType: .default,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "Product Name")
        }
    }
    
    @IBOutlet weak var btnProductCategory:UIButton!
    @IBOutlet weak var txtProductCategory:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtProductCategory,
                              tfName: txtProductCategory.text!,
                              tfCornerRadius: 6,
                              tfpadding: 20,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .clear,
                              tfAppearance: .dark,
                              tfKeyboardType: .default,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "Product Category")
        }
    }
    
    @IBOutlet weak var btnProductSubCategory:UIButton!
    @IBOutlet weak var txtProductSubCategory:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtProductSubCategory,
                              tfName: txtProductSubCategory.text!,
                              tfCornerRadius: 6,
                              tfpadding: 20,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .clear,
                              tfAppearance: .dark,
                              tfKeyboardType: .default,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "Product Sub Category")
        }
    }
    
    @IBOutlet weak var txtProductPrice:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtProductPrice,
                              tfName: txtProductPrice.text!,
                              tfCornerRadius: 6,
                              tfpadding: 20,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .clear,
                              tfAppearance: .dark,
                              tfKeyboardType: .default,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "Product Price")
        }
    }
    
    @IBOutlet weak var txtProductSpecialPrice:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtProductSpecialPrice,
                              tfName: txtProductSpecialPrice.text!,
                              tfCornerRadius: 6,
                              tfpadding: 20,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .clear,
                              tfAppearance: .dark,
                              tfKeyboardType: .default,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "Product Special Price")
        }
    }
    
    @IBOutlet weak var txtProductQuantity:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtProductQuantity,
                              tfName: txtProductQuantity.text!,
                              tfCornerRadius: 6,
                              tfpadding: 20,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .clear,
                              tfAppearance: .dark,
                              tfKeyboardType: .default,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "Product Quantity")
        }
    }
    
    @IBOutlet weak var txtProductSKU:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtProductSKU,
                              tfName: txtProductSKU.text!,
                              tfCornerRadius: 6,
                              tfpadding: 20,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .clear,
                              tfAppearance: .dark,
                              tfKeyboardType: .default,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "Product SKU")
        }
    }
    
    
    
    @IBOutlet weak var txtView:UITextView! {
        didSet {
            txtView.layer.cornerRadius = 6
            txtView.clipsToBounds = true
            txtView.backgroundColor = .white
            txtView.text = "Description"
            txtView.textColor = .lightGray
        }
    }
    
    @IBOutlet weak var viewBG:UIView! {
        didSet {
            viewBG.backgroundColor = .white
            viewBG.layer.cornerRadius = 6
            viewBG.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var btnuploadProductImage:UIButton!
    @IBOutlet weak var imgProductView:UIImageView! {
        didSet {
            imgProductView.backgroundColor = .orange
            imgProductView.layer.cornerRadius = 6
            imgProductView.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var txtProductEnterWebsiteURL:UITextField! {
        didSet {
            Utils.textFieldUI(textField: txtProductEnterWebsiteURL,
                              tfName: txtProductEnterWebsiteURL.text!,
                              tfCornerRadius: 6,
                              tfpadding: 20,
                              tfBorderWidth: 0.8,
                              tfBorderColor: .clear,
                              tfAppearance: .dark,
                              tfKeyboardType: .default,
                              tfBackgroundColor: .white,
                              tfPlaceholderText: "Product Website URL")
        }
    }
    
    @IBOutlet weak var btnSaveAndContinue:UIButton! {
        didSet {
            btnSaveAndContinue.backgroundColor = NAVIGATION_COLOR
            btnSaveAndContinue.layer.cornerRadius = 6
            btnSaveAndContinue.clipsToBounds = true
            btnSaveAndContinue.setTitleColor(.white, for: .normal)
            btnSaveAndContinue.setTitle("Save & Continue", for: .normal)
        }
    }
    
    @IBOutlet weak var btnAddnewProduct:UIButton! {
        didSet {
            btnAddnewProduct.backgroundColor = .clear
            btnAddnewProduct.setTitle("+ Add new product", for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
