//
//  TGAddProduct.swift
//  TheGlobalHair
//
//  Created by Apple on 19/11/20.
//

import UIKit
import Alamofire

class TGAddProduct: UIViewController, UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

// ***************************************************************** // nav
                    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_COLOR
        }
    }
            
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.tintColor = NAVIGATION_BACK_COLOR
        }
    }
            
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "Add Products"
            lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
        }
    }
                    
// ***************************************************************** // nav
   
    let cellReuseIdentifier = "tGAddProductTableCell"
    
    // MARK:- SELECT GENDER -
    let regularFont = UIFont.systemFont(ofSize: 16)
    let boldFont = UIFont.boldSystemFont(ofSize: 16)
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = []
    var page : Int! = 1
    var loadMore : Int! = 1
    
    var dummyArray:NSMutableArray! = []
    var arrProductCategoryName:NSMutableArray! = []
    var arrProductCategoryNameId:NSMutableArray! = []
    
    var arrProductSubCategory:NSMutableArray! = []
    var arrProductSubFullCategory:NSMutableArray! = []
    var arrProductSubFullCategoryName:NSMutableArray! = []
    var arrProductSubFullCategoryId:NSMutableArray! = []
    
    var strProductCategoryId:String!
    var strProductSubCategoryId:String!
    
    var imageStr1:String!
    var imgData1:Data!
    
    @IBOutlet weak var segmentContol:UISegmentedControl! {
        didSet {
            let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
            UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes, for: .selected)
            
            let titleTextAttributes2 = [NSAttributedString.Key.foregroundColor: UIColor.white]
            UISegmentedControl.appearance().setTitleTextAttributes(titleTextAttributes2, for: .normal)
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
// ***************************************************************** //
// ***************************************************************** //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = .black
        
        self.tbleView.separatorColor = .clear
    
        self.btnBack.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        self.imageStr1 = "0"
        
        
        self.pulishUnpulishedItem()
        // self.segmentContol.addTarget(self, action: #selector(switchhClickMethod), for: .valueChanged)
        
        // self.addMyProductsListing(statusText: "1")
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }

    @objc func switchhClickMethod() {
        
        if self.segmentContol.selectedSegmentIndex == 0 {
            
        } else {
            
            let alert = UIAlertController(title: String("Confirmation"), message: String("Are you sure you want to Un-Publish this item because if you do your product will not visible to anyone."), preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                
            }))
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                self.segmentContol.selectedSegmentIndex = 0
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Description"
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    @objc func addMyProductsListing(statusText:String) {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        // self.arrListOfAllMyOrders.removeAllObjects()
        
        self.view.endEditing(true)
        
        let params = EditPublishedProduct(action: "editproduct",
                                          productId: "",
                                          status: String(statusText))
        
        print(params as Any)
        
        AF.request(BASE_URL_THE_GLOBAL_HAIR,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                             ERProgressHud.sharedInstance.hide()
                           
                             var ar : NSArray!
                             ar = (JSON["data"] as! Array<Any>) as NSArray
                             self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            // self.totalItemsInCart()
                            
                            for categoryName in 0..<self.arrListOfAllMyOrders.count {
                                
                                let item = self.arrListOfAllMyOrders[categoryName] as! [String:Any]
                                
                                self.arrProductCategoryName.add(item["name"] as! String)
                                
                                let x : Int = (item["id"] as! Int)
                                let myString = String(x)
                                self.arrProductCategoryNameId.add(myString)
                                
                            }
                            
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
                   }
        
    }
    
    @objc func pulishUnpulishedItem() {
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        // self.arrListOfAllMyOrders.removeAllObjects()
        
        self.view.endEditing(true)
        
        let params = AddProductCategory(action: "category",
                                        pageNo: "1")
        print(params as Any)
        
        AF.request(BASE_URL_THE_GLOBAL_HAIR,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                             ERProgressHud.sharedInstance.hide()
                           
                             var ar : NSArray!
                             ar = (JSON["data"] as! Array<Any>) as NSArray
                             self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            // self.totalItemsInCart()
                            
                            for categoryName in 0..<self.arrListOfAllMyOrders.count {
                                
                                let item = self.arrListOfAllMyOrders[categoryName] as! [String:Any]
                                
                                self.arrProductCategoryName.add(item["name"] as! String)
                                
                                let x : Int = (item["id"] as! Int)
                                let myString = String(x)
                                self.arrProductCategoryNameId.add(myString)
                                
                            }
                            
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
                   }
        
    }
    
    @IBAction func changeSelectedIndex(sender: IGSwitch) {
      // selectedIndexLabel.text = "Selected Index: \(sender.selectedIndex)"
        
        if sender.selectedIndex == 0 {
            
            
        } else {
            print("Selected Index: \(sender.selectedIndex)")
            
            
            
        }
    }
    
    @objc func uploadProductWithoutImage () {
        
        if self.imageStr1 == "0" {
            
            // self.uploadWithNoImage()
            
            let alert = UIAlertController(title: String("Alert"), message: String("Please Upload product Image"), preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                
             }))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            
            self.validationBeforeUploadProduct()
        }
        
    }
    
    @objc func validationBeforeUploadProduct() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! TGAddProductTableCell
        
        /*
         productName: String(cell.txtProductName.text!),
         price: String(cell.txtProductPrice.text!),
         specialPrice: String(cell.txtProductSpecialPrice.text!),
         description: String(cell.txtView.text!),
         SKU: String(cell.txtProductSKU.text!),
         quantity: String(cell.txtProductQuantity.text!),
         URL: String(cell.txtProductEnterWebsiteURL.text!),
         categoryId: self.strProductCategoryId,
         subCategoryId: self.strProductSubCategoryId)
         */
        
        if cell.txtProductName.text == "" {
            self.popup(strFieldName: "Product Name")
        } else if cell.txtProductCategory.text == "" {
            self.popup(strFieldName: "Product Category")
        } else if cell.txtProductSubCategory.text == "" {
            self.popup(strFieldName: "Product Sub Category")
        } else if cell.txtProductPrice.text == "" {
            self.popup(strFieldName: "Product Price")
        } else if cell.txtProductSpecialPrice.text == "" {
            self.popup(strFieldName: "Product Special Price")
        } else if cell.txtProductQuantity.text == "" {
            self.popup(strFieldName: "Product Quantity")
        } else if cell.txtProductSKU.text == "" {
            self.popup(strFieldName: "Product SKU")
        } else if cell.txtView.text == "" {
            self.popup(strFieldName: "Product Description")
        } else if cell.txtProductEnterWebsiteURL.text == "" {
            self.popup(strFieldName: "Product Website URL")
        }  else {
            
            let string2 = cell.txtProductEnterWebsiteURL.text
            let character2: String = "http"
            if string2!.contains(character2) {
                print("yes http")
                
                
            } else {
                let addHTTP : String = "http://"+cell.txtProductEnterWebsiteURL.text!
                if verifyUrl(urlString: addHTTP) {
                    print("good http")
                    
                    let string3 = addHTTP
                    let character3: String = ".com"
                    
                    if string3.contains(character3) {
                        print("yes .com")
                        
                        if String(cell.txtProductPrice.text!) > String(cell.txtProductSpecialPrice.text!) {
                            self.uploadProductWIthImage()
                        } else {
                            let alert = UIAlertController(title: String("Alert"), message: String("Special price should not be greater than Price."), preferredStyle: UIAlertController.Style.alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { action in
                             }))
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        
                        
                        
                        
                        
                    } else {
                        let alert = UIAlertController(title: String("Error"), message: String("URL is in bad Format."), preferredStyle: UIAlertController.Style.alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { action in
                         }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                } else {
                    let alert = UIAlertController(title: String("Error"), message: String("URL is in bad Format."), preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { action in
                     }))
                    self.present(alert, animated: true, completion: nil)
                }
                
                
                
                
            }
            
            
            
        }
        
    }
    
    @objc func popup(strFieldName:String) {
        
        let alert = UIAlertController(title: String("Alert"), message: strFieldName+String(" should not be empty"), preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .destructive, handler: { action in
         }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // MARK:- WEBSERVICE ( ADD PRODUCT WITHOUT IMAGE ) -
    @objc func uploadWithNoImage() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! TGAddProductTableCell
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
        
        self.view.endEditing(true)
        
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         let x : Int = (person["userId"] as! Int)
         let myString = String(x)
            
            let params = SellerAddProductWithoutImage(action: "addproduct",
                                                      userId: String(myString),
                                                      productName: String(cell.txtProductName.text!),
                                                      price: String(cell.txtProductPrice.text!),
                                                      specialPrice: String(cell.txtProductSpecialPrice.text!),
                                                      description: String(cell.txtView.text!),
                                                      SKU: String(cell.txtProductSKU.text!),
                                                      quantity: String(cell.txtProductQuantity.text!),
                                                      URL: String(cell.txtProductEnterWebsiteURL.text!),
                                                      categoryId: self.strProductCategoryId,
                                                      subCategoryId: self.strProductSubCategoryId)
        
        AF.request(BASE_URL_THE_GLOBAL_HAIR,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        print(JSON as Any)
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                         var strSuccess2 : String!
                         strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("Success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            let alert = UIAlertController(title: String("Success"), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Add more product", style: .default, handler: { action in
                                
                                cell.txtProductName.text = ""
                                cell.txtProductPrice.text = ""
                                cell.txtProductSpecialPrice.text = ""
                                cell.txtView.text = ""
                                cell.txtProductSKU.text = ""
                                cell.txtProductQuantity.text = ""
                                cell.txtProductEnterWebsiteURL.text = ""
                                
                                self.strProductCategoryId = nil
                                self.strProductSubCategoryId = nil
                                
                                self.imageStr1 = "0"
                                
                                self.view.endEditing(true)
                                self.tbleView.setContentOffset(.zero, animated:true)
                                
                                // self.navigationController?.popViewController(animated: true)
                             }))
                            alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                                self.navigationController?.popViewController(animated: true)
                             }))
                            self.present(alert, animated: true, completion: nil)
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
        
    }
}
    
    
    @objc func cellTappedMethod1(){
            // print("you tap image number: \(sender.view.tag)")
           
           let alert = UIAlertController(title: "Upload Profile Image", message: nil, preferredStyle: .actionSheet)

           alert.addAction(UIAlertAction(title: "Camera", style: .default , handler:{ (UIAlertAction)in
               print("User click Approve button")
               self.openCamera1()
           }))

           alert.addAction(UIAlertAction(title: "Gallery", style: .default , handler:{ (UIAlertAction)in
               print("User click Edit button")
               self.openGallery1()
           }))

           alert.addAction(UIAlertAction(title: "In-Appropriate terms", style: .default , handler:{ (UIAlertAction)in
               print("User click Delete button")
           }))

           alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
               print("User click Dismiss button")
           }))

           self.present(alert, animated: true, completion: {
               print("completion block")
           })
           
       }
       
       @objc func openCamera1() {
           let imagePicker = UIImagePickerController()
           imagePicker.delegate = self
           imagePicker.sourceType = .camera;
           imagePicker.allowsEditing = false
           self.present(imagePicker, animated: true, completion: nil)
           
       }
       
       @objc func openGallery1() {
           let imagePicker = UIImagePickerController()
           imagePicker.delegate = self
           imagePicker.sourceType = .photoLibrary;
           imagePicker.allowsEditing = false
           self.present(imagePicker, animated: true, completion: nil)
           
       }
       
       internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
           
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! TGAddProductTableCell
        
        cell.imgProductView.isHidden = false
        let image_data = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        cell.imgProductView.image = image_data // show image on profile
        let imageData:Data = image_data!.pngData()!
        imageStr1 = imageData.base64EncodedString()
        self.dismiss(animated: true, completion: nil)
        imgData1 = image_data!.jpegData(compressionQuality: 0.2)!
               //print(type(of: imgData)) // data
               
        self.imageStr1 = "1"
           
           
            // self.uploadDataWithImage()
       }
    
    // MARK:- WEBSERVICE ( ADD PRODUCT WITH IMAGE ) -
    @objc func uploadProductWIthImage() {
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! TGAddProductTableCell
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "Please wait...")
          //Set Your URL
           let api_url = BASE_URL_THE_GLOBAL_HAIR
           guard let url = URL(string: api_url) else {
               return
           }

            if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // let str:String = person["role"] as! String
           
               let x : Int = person["userId"] as! Int
               let myString = String(x)
               
               var urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 10.0 * 1000)
               urlRequest.httpMethod = "POST"
               urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")

                /*
                 let action: String
                 let userId: String
                 let productName: String
                 let price: String
                 let specialPrice: String
                 let description: String
                 let SKU: String
                 let quantity: String
                 let URL: String
                 let categoryId: String
                 let subCategoryId: String
                 */
                
           //Set Your Parameter
               let parameterDict = NSMutableDictionary()
               parameterDict.setValue("addproduct", forKey: "action")
               parameterDict.setValue(String(myString), forKey: "userId")
               parameterDict.setValue(String(cell.txtProductName.text!), forKey: "productName")
               parameterDict.setValue(String(cell.txtProductPrice.text!), forKey: "price")
               parameterDict.setValue(String(cell.txtProductSpecialPrice.text!), forKey: "specialPrice")
                parameterDict.setValue(String(cell.txtView.text!), forKey: "description")
                parameterDict.setValue(String(cell.txtProductSKU.text!), forKey: "SKU")
                parameterDict.setValue(String(cell.txtProductQuantity.text!), forKey: "quantity")
                parameterDict.setValue(String(cell.txtProductEnterWebsiteURL.text!), forKey: "URL")
                parameterDict.setValue(String(self.strProductCategoryId), forKey: "categoryId")
                parameterDict.setValue(String(self.strProductSubCategoryId), forKey: "subCategoryId")

                print(parameterDict as Any)
                
           //Set Image Data
           // let imgData = self.img_photo.image!.jpegData(compressionQuality: 0.5)!

           /*
            let params = EditUserWithoutImage(action: "editprofile",
            userId: String(myString),
            fullName: String(cell.txtUsername.text!),
            contactNumber: String(cell.txtPhoneNumber.text!),
            address: String(cell.txtAddress.text!))
            */
                
          // Now Execute
           AF.upload(multipartFormData: { multiPart in
               for (key, value) in parameterDict {
                   if let temp = value as? String {
                       multiPart.append(temp.data(using: .utf8)!, withName: key as! String)
                   }
                   if let temp = value as? Int {
                       multiPart.append("\(temp)".data(using: .utf8)!, withName: key as! String)
                   }
                   if let temp = value as? NSArray {
                       temp.forEach({ element in
                           let keyObj = key as! String + "[]"
                           if let string = element as? String {
                               multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                           } else
                               if let num = element as? Int {
                                   let value = "\(num)"
                                   multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                           }
                       })
                   }
               }
               multiPart.append(self.imgData1, withName: "image", fileName: "addProductImage.png", mimeType: "image/png")
           }, with: urlRequest)
               .uploadProgress(queue: .main, closure: { progress in
                   //Current upload progress of file
                   print("Upload Progress: \(progress.fractionCompleted)")
               })
               .responseJSON(completionHandler: { data in

                          switch data.result {

                          case .success(_):
                           do {
                           
                           let dictionary = try JSONSerialization.jsonObject(with: data.data!, options: .fragmentsAllowed) as! NSDictionary
                             
                               print("Success!")
                               print(dictionary)
                            
                            var strSuccess2 : String!
                            strSuccess2 = dictionary["msg"]as Any as? String
                            
                            self.imageStr1 = "0"
                            
                               ERProgressHud.sharedInstance.hide()
                               
                            
                            let alert = UIAlertController(title: String("Success"), message: String(strSuccess2), preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Add more product", style: .default, handler: { action in
                                
                                cell.txtProductName.text = ""
                                cell.txtProductPrice.text = ""
                                cell.txtProductSpecialPrice.text = ""
                                cell.txtView.text = ""
                                cell.txtProductSKU.text = ""
                                cell.txtProductQuantity.text = ""
                                cell.txtProductEnterWebsiteURL.text = ""
                                
                                self.strProductCategoryId = nil
                                self.strProductSubCategoryId = nil
                                
                                self.imageStr1 = "0"
                                
                                self.view.endEditing(true)
                                self.tbleView.setContentOffset(.zero, animated:true)
                                
                                // self.navigationController?.popViewController(animated: true)
                            }))
                           alert.addAction(UIAlertAction(title: "Dismiss", style: .destructive, handler: { action in
                               self.navigationController?.popViewController(animated: true)
                            }))
                           self.present(alert, animated: true, completion: nil)
                            
                          }
                          catch {
                             // catch error.
                           print("catch error")
                           ERProgressHud.sharedInstance.hide()
                                 }
                           break
                               
                          case .failure(_):
                           print("failure")
                           ERProgressHud.sharedInstance.hide()
                           break
                           
                       }


               })
           
       }}
}


extension TGAddProduct: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:TGAddProductTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! TGAddProductTableCell
          
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.txtView.delegate = self
        
        cell.btnProductCategory.addTarget(self, action: #selector(productCategoryListing), for: .touchUpInside)
        cell.btnProductSubCategory.addTarget(self, action: #selector(productSubCategoryListing), for: .touchUpInside)
        
        cell.btnSaveAndContinue.addTarget(self, action: #selector(uploadProductWithoutImage), for: .touchUpInside)
        
        cell.btnuploadProductImage.addTarget(self, action: #selector(cellTappedMethod1), for: .touchUpInside)
        
        /*
        // image one
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(TGAddProduct.cellTappedMethod1(_:)))

        cell.imgProductView.isUserInteractionEnabled = true
        // imgProfile.tag = indexPath.row
        cell.imgProductView.addGestureRecognizer(tapGestureRecognizer1)
        */
        
        cell.backgroundColor = .clear
        
        return cell
        
    }
    
    @objc func productCategoryListing() {
        self.arrProductSubCategory.removeAllObjects()
        self.arrProductSubFullCategory.removeAllObjects()
        self.arrProductSubFullCategoryName.removeAllObjects()
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! TGAddProductTableCell
        
        // print(self.arrListOfAllMyOrders as Any)
        
        
        
        let redAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "Select Product Category",
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : NAVIGATION_COLOR,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "search product category",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Okay",
            doneButtonColor     : APP_BASIC_COLOR,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"red_ic_checked"),
            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
             
            // MARK:- CONVERT MUTABLE ARRAY TO ARRAY -
        let array: [String] = self.arrProductCategoryName.copy() as! [String]
            
        let arrGender = array
        let picker = YBTextPicker.init(with: arrGender, appearance: redAppearance,
                                       onCompletion: { (selectedIndexes, selectedValues) in
                                        if let selectedValue = selectedValues.first{
                                            if selectedValue == arrGender.last!{
                                                     
                                                let myArray = selectedIndexes //[1,5,2]
                                                var myString = ""
                                                _ = myArray.map{ myString = myString + "\($0)" }
                                                let myInt = Int(myString)
                                                
                                                self.arrProductSubCategory.add(self.arrListOfAllMyOrders[myInt!])
                                                
                                                cell.txtProductCategory.text = "\(selectedValue)"
                                                
                                                self.tbleView.reloadData()
                                                    
                                            } else {
                                                     
                                                // print(selectedIndexes as Any)
                                                // print(type(of: selectedIndexes))
                                                
                                                let myArray = selectedIndexes //[1,5,2]
                                                var myString = ""
                                                _ = myArray.map{ myString = myString + "\($0)" }
                                                let myInt = Int(myString)
                                                
                                                self.arrProductSubCategory.add(self.arrListOfAllMyOrders[myInt!])
                                                
                                                // var dict: Dictionary<AnyHashable, Any>
                                                // dict = JSON["data"] as! Dictionary<AnyHashable, Any>
                                                
                                                cell.txtProductCategory.text = "\(selectedValue)"
                                                
                                                self.tbleView.reloadData()
                                            }
                                        } else {
                                                 // self.btnGenderPicker.setTitle("What's your gender?", for: .normal)
                                                  cell.txtProductCategory.text = "Please select your country"
                                                
                                            print()
                                                
                                        }
                                       },
                                       onCancel: {
                                        print("Cancelled")
                                       })
             
        picker.show(withAnimation: .FromBottom)
    }
    
    @objc func productSubCategoryListing() {
        // self.arrProductSubCategory.removeAllObjects()
        self.arrProductSubFullCategory.removeAllObjects()
        self.arrProductSubFullCategoryName.removeAllObjects()
        
        let indexPath = IndexPath.init(row: 0, section: 0)
        let cell = self.tbleView.cellForRow(at: indexPath) as! TGAddProductTableCell
        
        // print(self.arrProductSubCategory as Any)
        
        var ar : NSArray!
        for categoryName in 0..<self.arrProductSubCategory.count {
            
            let item = self.arrProductSubCategory[categoryName] as! [String:Any]
            
            ar = (item["SubCat"] as! Array<Any>) as NSArray
            
            self.arrProductSubFullCategory.addObjects(from: ar as! [Any])
            
            // get product category id
            let x : Int = (item["id"] as! Int)
            let myString = String(x)
            self.strProductCategoryId = myString
            // print(self.strProductCategoryId as Any)
            
            for subCategoryName in 0..<self.arrProductSubFullCategory.count {
                let item = self.arrProductSubFullCategory[subCategoryName] as! [String:Any]
                 // print(item["name"] as! String)
                
                // let x : Int = (item["id"] as! Int)
                // let myString = String(x)
                // self.strProductSubCategoryId = myString
                
                self.arrProductSubFullCategoryName.add((item["name"] as! String))
                
                
            }
            
        }
        
        //  print(self.arrProductSubFullCategoryName as Any)
        
        let redAppearance = YBTextPickerAppearanceManager.init(
            pickerTitle         : "Select Product Sub Category",
            titleFont           : boldFont,
            titleTextColor      : .white,
            titleBackground     : NAVIGATION_COLOR,
            searchBarFont       : regularFont,
            searchBarPlaceholder: "search product Sub category",
            closeButtonTitle    : "Cancel",
            closeButtonColor    : .darkGray,
            closeButtonFont     : regularFont,
            doneButtonTitle     : "Okay",
            doneButtonColor     : APP_BASIC_COLOR,
            doneButtonFont      : boldFont,
            checkMarkPosition   : .Right,
            itemCheckedImage    : UIImage(named:"red_ic_checked"),
            itemUncheckedImage  : UIImage(named:"red_ic_unchecked"),
            itemColor           : .black,
            itemFont            : regularFont
        )
             
            // MARK:- CONVERT MUTABLE ARRAY TO ARRAY -
        let array: [String] = self.arrProductSubFullCategoryName.copy() as! [String]
            
        let arrGender = array
        let picker = YBTextPicker.init(with: arrGender, appearance: redAppearance,
                                       onCompletion: { (selectedIndexes, selectedValues) in
                                        if let selectedValue = selectedValues.first{
                                            if selectedValue == arrGender.last!{
                                                   
                                                let myArray = selectedIndexes //[1,5,2]
                                                var myString = ""
                                                _ = myArray.map{ myString = myString + "\($0)" }
                                                let myInt = Int(myString)
                                                
                                                let item = self.arrProductSubFullCategory[myInt!] as! [String:Any]
                                                
                                                 let x : Int = (item["id"] as! Int)
                                                 let myString2 = String(x)
                                                 self.strProductSubCategoryId = myString2
                                                
                                                    // self.productQuantityWithTextSaved = "\(selectedValue)"
                                                cell.txtProductSubCategory.text = "\(selectedValue)"
                                                self.tbleView.reloadData()
                                                    
                                            } else {
                                                 
                                                let myArray = selectedIndexes //[1,5,2]
                                                var myString = ""
                                                _ = myArray.map{ myString = myString + "\($0)" }
                                                let myInt = Int(myString)
                                                
                                                let item = self.arrProductSubFullCategory[myInt!] as! [String:Any]
                                                
                                                 let x : Int = (item["id"] as! Int)
                                                 let myString2 = String(x)
                                                 self.strProductSubCategoryId = myString2
                                                
                                                    // self.productQuantityWithTextSaved = "\(selectedValue)"
                                                cell.txtProductSubCategory.text = "\(selectedValue)"
                                                self.tbleView.reloadData()
                                            }
                                        } else {
                                                 // self.btnGenderPicker.setTitle("What's your gender?", for: .normal)
                                                  cell.txtProductSubCategory.text = "Please select product sub category"
                                                
                                            print()
                                                
                                        }
                                       },
                                       onCancel: {
                                        print("Cancelled")
                                       })
             
        picker.show(withAnimation: .FromBottom)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1200 // UITableView.automaticDimension
    }
    
}

extension TGAddProduct: UITableViewDelegate {
    
}
