//
//  TGHNDashboard.swift
//  TheGlobalHair
//
//  Created by Apple on 19/11/20.
//

import UIKit
import Alamofire
import SDWebImage

class TGHNDashboard: UIViewController {

// ***************************************************************** // nav
            
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_COLOR
        }
    }
    
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.tintColor = NAVIGATION_BACK_COLOR
        }
    }
    
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "Dashbaord"
            lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
        }
    }
            
// ***************************************************************** // nav
    
    @IBOutlet weak var btnMenu:UIButton!
    
    @IBOutlet weak var lblAddress:UILabel! {
        didSet {
            lblAddress.textColor = APP_BASIC_COLOR
        }
    }
    
    @IBOutlet weak var btnEdit:UIButton!
    @IBOutlet weak var btnGroup:UIButton!
    
    @IBOutlet weak var btnManageProducts:UIButton! {
        didSet {
            btnManageProducts.backgroundColor = BUTTON_COLOR_BLUE
            btnManageProducts.layer.cornerRadius = 6
            btnManageProducts.clipsToBounds = true
            btnManageProducts.setTitleColor(.black, for: .normal)
            btnManageProducts.setTitle("Manage Products", for: .normal)
        }
    }
    
    
    
    @IBOutlet weak var imgVieww:UIImageView!
    
    @IBOutlet weak var switchh:UISwitch! {
        didSet {
            switchh.isHidden = true
        }
    }
    
    @IBOutlet weak var bottomVieww:UIView! {
        didSet {
            bottomVieww.backgroundColor = .clear
        }
    }
  
    @IBOutlet weak var lblUserName:UILabel! {
        didSet {
            lblUserName.textColor = APP_BASIC_COLOR
        }
    }
 
// ***************************************************************** //
// ***************************************************************** //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.btnManageProducts.addTarget(self, action: #selector(myProductsClickMethod), for: .touchUpInside)
        
        self.sideBarMenuClick()
        
        
        if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
            // print(person as Any)
            
            /*
             ["userId": 122, "deviceToken": 111111111111111111111, "drivlingImage": , "lastName": , "country": India, "middleName": , "BankName": , "socialType": , "contactNumber": 9785764567, "RoutingNo": , "latitude": , "wallet": 0, "dob": , "role": Seller, "socialId": , "longitude": , "AccountNo": , "AutoInsurance": , "zipCode": , "ssnImage": , "accountType": , "logitude": , "AccountHolderName": , "address": water, "gender": , "email": gh4@gmail.com, "state": , "image": , "fullName": gh 4, "firebaseId": , "device": iOS]
             */
            
            self.lblUserName.text = (person["fullName"] as! String)
            self.lblAddress.text = (person["address"] as! String)
            
            imgVieww.sd_setImage(with: URL(string: (person["image"] as! String)), placeholderImage: UIImage(named: "logo"))
        }
    }
    
    @objc func sideBarMenuClick() {
        
        self.view.endEditing(true)
        if revealViewController() != nil {
            btnMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
    }
    
    @objc func myProductsClickMethod() {
         let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TGMyProductsId") as? TGMyProducts
         self.navigationController?.pushViewController(push!, animated: true)
    }
    
}
