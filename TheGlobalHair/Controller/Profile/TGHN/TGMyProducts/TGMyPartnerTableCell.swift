//
//  TGMyPartnerTableCell.swift
//  TheGlobalHair
//
//  Created by Apple on 19/11/20.
//

import UIKit

class TGMyPartnerTableCell: UITableViewCell {

    @IBOutlet weak var imgProfile:UIImageView! {
        didSet {
            imgProfile.backgroundColor = .clear
            imgProfile.layer.cornerRadius = 8
            imgProfile.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblProductName:UILabel! {
        didSet {
            lblProductName.textColor = APP_BASIC_COLOR
            lblProductName.text = "Brazilian Straight hair\nColor : Black"
        }
    }
    
    @IBOutlet weak var lblProductPrice:UILabel! {
        didSet {
            lblProductPrice.textColor = .systemRed
            lblProductPrice.text = "Price : $200.99"
            
            let attrString = NSAttributedString(string: "Label Text", attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
            lblProductPrice.attributedText = attrString
        }
    }
    @IBOutlet weak var lblProductSpecialPrice:UILabel! {
        didSet {
            lblProductSpecialPrice.textColor = NAVIGATION_COLOR
            lblProductSpecialPrice.text = "Price : $200.99"
        }
    }
    
    @IBOutlet weak var btnEdit:UIButton! {
        didSet {
            btnEdit.layer.cornerRadius = 12
            btnEdit.clipsToBounds = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
