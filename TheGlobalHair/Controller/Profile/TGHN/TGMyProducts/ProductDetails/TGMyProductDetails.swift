//
//  TGMyProductDetails.swift
//  TheGlobalHair
//
//  Created by Apple on 19/11/20.
//

import UIKit

class TGMyProductDetails: UIViewController {

// ***************************************************************** // nav
                    
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_COLOR
        }
    }
            
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.tintColor = NAVIGATION_BACK_COLOR
        }
    }
            
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "Product Details"
            lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
        }
    }
                    
// ***************************************************************** // nav
    
    let cellReuseIdentifier = "tGMyProductDetailsTableCell"
    
    var dictGetMyProductDetails:NSDictionary!
    
    @IBOutlet weak var btnMenu:UIButton! {
        didSet {
            btnMenu.tintColor = NAVIGATION_BACK_COLOR
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            self.tbleView.delegate = self
            self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
// ***************************************************************** //
// ***************************************************************** //
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = .black
        
        self.btnMenu.addTarget(self, action: #selector(backClickMethod), for: .touchUpInside)
        
        // print(dictGetMyProductDetails as Any)
        
        /*
         SKU = "qwerty-123456";
         URL = "www.google.com";
         category = Curly;
         categoryId = 2;
         description = We;
         image = "http://demo2.evirtualservices.co/hair/site/img/uploads/products/1606396761addProductImage.png";
         price = 200;
         productId = 66;
         productName = "iOS 6";
         quantity = 4;
         specialPrice = 199;
         subCategoryId = 5;
         subcategory = "curly hair";
         */
        
        
    }
    
    @objc func backClickMethod() {
        self.navigationController?.popViewController(animated: true)
    }

}

extension TGMyProductDetails: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:TGMyProductDetailsTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! TGMyProductDetailsTableCell
          
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        cell.backgroundColor = .clear
        
        cell.lblProductName.text = (dictGetMyProductDetails!["productName"] as! String)
        
        // special price
        if dictGetMyProductDetails!["specialPrice"] is String {
            print("Yes, it's a String")

            cell.lblProductPrice.text = "Special Price : $ "+(dictGetMyProductDetails!["specialPrice"] as! String)

        } else if dictGetMyProductDetails!["specialPrice"] is Int {
            print("It is Integer")
                        
            let x2 : Int = (dictGetMyProductDetails!["specialPrice"] as! Int)
            let myString2 = String(x2)
            cell.lblProductPrice.text = "Special Price : $ "+myString2
                        
        } else {
            print("i am number")
                        
            let temp:NSNumber = dictGetMyProductDetails!["specialPrice"] as! NSNumber
            let tempString = temp.stringValue
            cell.lblProductPrice.text = "Special Price : $ "+tempString
        }
        
        cell.lblSKU.text = "SKU : "+(dictGetMyProductDetails!["SKU"] as! String)
        
        cell.lblCategory.text = "CATEGORY / SUB-CATEGORY :"+(dictGetMyProductDetails!["category"] as! String)+"/"+(dictGetMyProductDetails!["subcategory"] as! String)
        
        cell.lblDescription.text = (dictGetMyProductDetails!["description"] as! String)
        
        cell.imgProductImage.sd_setImage(with: URL(string: (dictGetMyProductDetails!["image"] as! String)), placeholderImage: UIImage(named: "logo"))
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return UITableView.automaticDimension
    }
    
}

extension TGMyProductDetails: UITableViewDelegate {
    
}
