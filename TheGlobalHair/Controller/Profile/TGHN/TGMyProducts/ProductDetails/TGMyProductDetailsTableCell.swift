//
//  TGMyProductDetailsTableCell.swift
//  TheGlobalHair
//
//  Created by Apple on 19/11/20.
//

import UIKit

class TGMyProductDetailsTableCell: UITableViewCell {

    @IBOutlet weak var imgProductImage:UIImageView! {
        didSet {
            imgProductImage.backgroundColor = .black
            imgProductImage.layer.cornerRadius = 4
            imgProductImage.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lblProductName:UILabel! {
        didSet {
            lblProductName.textColor = APP_BASIC_COLOR
        }
    }
    
    @IBOutlet weak var lblProductPrice:UILabel! {
        didSet {
            lblProductPrice.text = "Price : $299.9"
            lblProductPrice.textColor = APP_BASIC_COLOR
        }
    }
    
    @IBOutlet weak var btnTryVirtually:UIButton! {
        didSet {
            btnTryVirtually.setTitle("Try this hair virtually", for: .normal)
            btnTryVirtually.setTitleColor(.white, for: .normal)
        }
    }
    
    @IBOutlet weak var lblSKU:UILabel! {
        didSet {
            lblSKU.text = "SKU : JHVC678D!"
            lblSKU.textColor = APP_BASIC_COLOR
        }
    }
    
    @IBOutlet weak var lblShipping:UILabel! {
        didSet {
            lblShipping.text = "Shipping : Free shipping"
            lblShipping.textColor = .systemGreen
        }
    }
    
    @IBOutlet weak var lblCategory:UILabel! {
        didSet {
            lblCategory.text = "Category : CBD Oils"
            lblCategory.textColor = .white
        }
    }
    
    @IBOutlet weak var lblDescriptionTitle:UILabel! {
        didSet {
            lblDescriptionTitle.text = "Description"
            lblDescriptionTitle.textColor = .white
        }
    }
    
    @IBOutlet weak var lblDescription:UILabel! {
        didSet {
            lblDescription.text = "Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description "
            lblDescription.textColor = .white
            lblDescription.backgroundColor = .clear
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
