//
//  TGMyProducts.swift
//  TheGlobalHair
//
//  Created by Apple on 19/11/20.
//

import UIKit
import Alamofire

class TGMyProducts: UIViewController {

// ***************************************************************** // nav
                
    @IBOutlet weak var navigationBar:UIView! {
        didSet {
            navigationBar.backgroundColor = NAVIGATION_COLOR
        }
    }
        
    @IBOutlet weak var btnBack:UIButton! {
        didSet {
            btnBack.tintColor = NAVIGATION_BACK_COLOR
        }
    }
        
    @IBOutlet weak var lblNavigationTitle:UILabel! {
        didSet {
            lblNavigationTitle.text = "My Products"
            lblNavigationTitle.textColor = NAVIGATION_TITLE_COLOR
        }
    }
                
// ***************************************************************** // nav
    
    let cellReuseIdentifier = "tGMyPartnerTableCell"
    
    @IBOutlet weak var btnMenu:UIButton! {
        didSet {
            btnMenu.tintColor = .white
        }
    }
    
    @IBOutlet weak var btnPlus:UIButton! {
        didSet {
            btnPlus.tintColor = NAVIGATION_BACK_COLOR
        }
    }
    
    // MARK:- TABLE VIEW -
    @IBOutlet weak var tbleView: UITableView! {
        didSet {
            // self.tbleView.delegate = self
            // self.tbleView.dataSource = self
            self.tbleView.backgroundColor = .clear
            self.tbleView.tableFooterView = UIView.init(frame: CGRect(origin: .zero, size: .zero))
        }
    }
    
// ***************************************************************** //
// ***************************************************************** //
    
    // MARK:- ARRAY -
    var arrListOfAllMyOrders:NSMutableArray! = [] // Array<Any>!
    var page : Int! = 1
    var loadMore : Int! = 1;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.view.backgroundColor = .black
        
        self.tbleView.separatorColor = .white
        
        self.btnPlus.addTarget(self, action: #selector(plusClickMethod), for: .touchUpInside)
        
        self.sideBarMenuClick()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.arrListOfAllMyOrders.removeAllObjects()
        self.addMyProductList(pageNumber: 1)
    }
    
    @objc func sideBarMenuClick() {
        
        self.view.endEditing(true)
        if revealViewController() != nil {
            btnMenu.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            revealViewController().rearViewRevealWidth = 300
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
    }
    
    @objc func plusClickMethod() {
        
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TGAddProductId") as? TGAddProduct
        self.navigationController?.pushViewController(push!, animated: true)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
                
        if scrollView == self.tbleView {
            let isReachingEnd = scrollView.contentOffset.y >= 0
                && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
            if(isReachingEnd) {
                if(loadMore == 1) {
                    loadMore = 0
                    page += 1
                    print(page as Any)
                    
                    self.addMyProductList(pageNumber: page)
                    
                }
            }
        }
    }
    
    // MARK:- WEBSERVICE ( ADD MY PRODUCT LIST ) -
    @objc func addMyProductList(pageNumber:Int) {
        
        ERProgressHud.sharedInstance.showDarkBackgroundView(withTitle: "fetching your products...")
        
        self.view.endEditing(true)
        
         if let person = UserDefaults.standard.value(forKey: "keyLoginFullData") as? [String:Any] {
         let x : Int = (person["userId"] as! Int)
         let myString = String(x)
            
            let params = MyProductsList(action: "productlist",
                                        userId: String(myString),
                                        categoryId: "",
                                        pageNo: pageNumber)
        
        AF.request(BASE_URL_THE_GLOBAL_HAIR,
                   method: .post,
                   parameters: params,
                   encoder: JSONParameterEncoder.default).responseJSON { response in
                    // debugPrint(response.result)
                    
                    switch response.result {
                    case let .success(value):
                        
                        let JSON = value as! NSDictionary
                        // print(JSON as Any) 
                        
                        var strSuccess : String!
                        strSuccess = JSON["status"]as Any as? String
                        
                        // var strSuccess2 : String!
                        // strSuccess2 = JSON["msg"]as Any as? String
                        
                        if strSuccess == String("success") {
                            print("yes")
                            ERProgressHud.sharedInstance.hide()
                           
                            var ar : NSArray!
                            ar = (JSON["data"] as! Array<Any>) as NSArray
                            self.arrListOfAllMyOrders.addObjects(from: ar as! [Any])
                            
                            self.tbleView.delegate = self
                            self.tbleView.dataSource = self
                            self.tbleView.reloadData()
                            
                        } else {
                            print("no")
                            ERProgressHud.sharedInstance.hide()
                            
                            var strSuccess2 : String!
                            strSuccess2 = JSON["msg"]as Any as? String
                            
                            Utils.showAlert(alerttitle: String(strSuccess), alertmessage: String(strSuccess2), ButtonTitle: "Ok", viewController: self)
                            
                        }
                        
                    case let .failure(error):
                        print(error)
                        ERProgressHud.sharedInstance.hide()
                        
                        Utils.showAlert(alerttitle: SERVER_ISSUE_TITLE, alertmessage: SERVER_ISSUE_MESSAGE, ButtonTitle: "Ok", viewController: self)
                    }
        }
        
    }
}
    
}

extension TGMyProducts: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListOfAllMyOrders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell:TGMyPartnerTableCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! TGMyPartnerTableCell
          
        let backgroundView = UIView()
        backgroundView.backgroundColor = .clear
        cell.selectedBackgroundView = backgroundView
        
        /*
         SKU = "qwerty-123456";
         URL = "www.google.com";
         category = Curly;
         categoryId = 2;
         description = We;
         image = "http://demo2.evirtualservices.co/hair/site/img/uploads/products/1606396761addProductImage.png";
         price = 200;
         productId = 66;
         productName = "iOS 6";
         quantity = 4;
         specialPrice = 199;
         subCategoryId = 5;
         subcategory = "curly hair";
         */
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        cell.lblProductName.text = (item!["productName"] as! String)+"\nSKU : "+(item!["SKU"] as! String)
        // cell.lblProductName.text = (item!["SKU"] as! String)
        
        // price
        if item!["price"] is String {
            print("Yes, it's a String")

            cell.lblProductPrice.text = "Price : $ "+(item!["price"] as! String)

        } else if item!["price"] is Int {
            print("It is Integer")
                        
            let x2 : Int = (item!["price"] as! Int)
            let myString2 = String(x2)
            cell.lblProductPrice.text = "Price : $ "+myString2
                        
        } else {
            print("i am number")
                        
            let temp:NSNumber = item!["price"] as! NSNumber
            let tempString = temp.stringValue
            cell.lblProductPrice.text = "Price : $ "+tempString
        }
        
        // special price
        if item!["specialPrice"] is String {
            print("Yes, it's a String")

            cell.lblProductSpecialPrice.text = "Special Price : $ "+(item!["specialPrice"] as! String)

        } else if item!["specialPrice"] is Int {
            print("It is Integer")
                        
            let x2 : Int = (item!["specialPrice"] as! Int)
            let myString2 = String(x2)
            cell.lblProductSpecialPrice.text = "Special Price : $ "+myString2
                        
        } else {
            print("i am number")
                        
            let temp:NSNumber = item!["specialPrice"] as! NSNumber
            let tempString = temp.stringValue
            cell.lblProductSpecialPrice.text = "Special Price : $ "+tempString
        }
        
        cell.imgProfile.sd_setImage(with: URL(string: (item!["image"] as! String)), placeholderImage: UIImage(named: "logo"))
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(editProductClickMethod), for: .touchUpInside)
        
        cell.backgroundColor = .clear
        
        return cell
        
    }
    
    @objc func editProductClickMethod(_ sender:UIButton) {
        
        let item = arrListOfAllMyOrders[sender.tag] as? [String:Any]
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TGEditProductId") as? TGEditProduct
        push!.dictGetProductDetails = item as NSDictionary?
        self.navigationController?.pushViewController(push!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView .deselectRow(at: indexPath, animated: true)
        
        let item = arrListOfAllMyOrders[indexPath.row] as? [String:Any]
        let push = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TGMyProductDetailsId") as? TGMyProductDetails
        push!.dictGetMyProductDetails = item as NSDictionary?
        self.navigationController?.pushViewController(push!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return 100 // UITableView.automaticDimension
    }
    
}

extension TGMyProducts: UITableViewDelegate {
    
}
